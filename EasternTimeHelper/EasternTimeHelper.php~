<?php

/**
 * Helper to get Eastern Standard Time used in USA and Canada.
 * 
 * @link https://en.wikipedia.org/wiki/Eastern_Time_Zone WikipediA descripton.
 * So local times change at 2:00 a.m. EST to 3:00 a.m. EDT on the second Sunday 
 * in March and return at 2:00 a.m. EDT to 1:00 a.m. EST on the first Sunday in
 * November. In Canada, the time changes as it does in the United States.
 */
class EasternTimeHelper
{
    /**
     * Offset of EST time from UTC+0/GMT.
     */
    const EST_OFFSET_SECONDS = -18000;
    
    /**
     * Offset of EDT time from UTC+0/GMT.
     */
    const EDT_OFFSET_SECONDS = -14400;
    
    /**
     * Time offset in date when EST changes to EDT and vice versa.
     */
    const ES_TIME_CHANGE = 7200;

    /**
     * Returns EST or EDT depending on current season.
     * 
     * @param $format String Format of date.
     */
    public function getEasternTime($format="Y-m-d H:i:s") {
        if ($this->inEST()) {
            return $format === true ? $this->getEST() : date($format, $this->getEST());
        }
        return $format === true ? $this->getEDT() : date($format, $this->getEDT());
    }
    
    /**
     * Return EST no matter on season.
     * 
     * @param $format String Format of date.
     */
    public function getEST() {
        return time() + self::EST_OFFSET_SECONDS;
    }
    
    /**
     * Return EDT no matter on season.
     * 
     * @param $format String Format of date.
     */
    public function getEDT() {
        return time() + self::EDT_OFFSET_SECONDS;
    }
    
    /**
     * Is current season EST.
     * 
     * @return boolean Wheather current season time is EST, false if it is EDT.
     */
    public function inEST() {
        $offset = self::ES_TIME_CHANGE;
        // Store current timezone set.
        $timezone_set = date_default_timezone_get();
        // Set timezone to UTC to simplify code.
        date_default_timezone_set("UTC");
        $est_time = $this->getEST();
        // Get current year.
        $year = date("Y", $est_time);
        // Get unixtime of this year when EST should change to EDT.
        $est_to_edt_time = strtotime("Second Sunday Of March {$year}") + $offset;
        // Get unixtime of this year when EDT should change to EST.
        $edt_to_est_time = strtotime("First Sunday Of November {$year}") + $offset;
        // Set default timezone to one was set before use.
        date_default_timezone_set($timezone_set);
        return  $est_time < $est_to_edt_time && $est_time > $edt_to_est_time;
    }
}
